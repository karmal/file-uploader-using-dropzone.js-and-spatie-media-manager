<?php

namespace App\Http\Controllers;

use App\User;
use App\File;
use Illuminate\Http\Request;

use function GuzzleHttp\Promise\all;

class UserNameController extends Controller
{


    public function index()
    {
        $users = User::latest()->get();
        $files = File::latest()->get();


        // I was thinking i also have to send data from files table to view but i dont think so coz i can just use relationship, user->file->something here

        return view('users.index', ['users' => $users, 'files' => $files]);
    }



    public function store(Request $request)
    {

        //Validate submitted data
        $request->validate([
            'username' => 'required|string',
            // 'file_ids' => 'required|array',
            // 'file_ids.*' => 'required|integer|exists:files,id',
            'file_id' => 'required|integer|exists:files,id',
        ]);

        $user = new User(); // Make Empty User Model
        $data = $request->all(); // Get everything the user sent in this request. 
        $user->fill($data); // Fill the user with the correct data (from fillable on the user model)
        $user->save(); //Save the user into the database. 
        if (isset($data['file_id'])) { // We will need to loop through each file that the user sends (update frontend first)
            $file = File::find($data['file_id']); // Find the file by file_id
            $file->user()->associate($user); // Relate the user you've just created to the file. 
            $file->save(); // Re-save the file (update).
        }
        return redirect('/files');
    }


    public function show($id)

    {

        $user = User::where('id', '=', $id)->first();

        return view('users.show', compact('user'));
    }

    public function destroy($id)

    {
        // got files id and removed file form database, now user with file_id
        $file = File::findOrFail($id)->delete();
        $user = User::where('file_id', '=', $id)->delete();


        return redirect('/files');
    }
}
