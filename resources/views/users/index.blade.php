@extends('layout')


@section('navbar')
  <div class="container navbar">
      <a href="/files/create">Create new upload</a>
      <a href="/files">Show Articles</a>
  </div>
@endsection


  @section('content')
    @foreach($files as $file)
<div class="container">
  <h2>username:{{$file->user->username}}</h2>
  <video width="320" height="240" controls>
    <source src="{{$file->url}}" type="video/mp4">
  </video>
<button type="button" class="btn"><a href="/files/{{$file->user->id}}">View</a></button>
  <form action="{{route('destroy',['file' => $file->user->id])}}" method="POST">
    @method('DELETE')
    @csrf
    <button type="submit">Delete</button>               
  </form>
</div> 




  @endforeach
@endsection












