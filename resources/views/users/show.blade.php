@extends('layout')

@section('navbar')
<div class="container navbar">
    <a href="/files/create">Create new upload</a>
    <a href="/files">Show Articles</a>
</div>
@endsection

@section('content')
    <div class="container">
        <h2>username:{{$user->username}}</h2>
        {{-- <video width="320" height="240" controls>
        <source src="{{$user->file->url}}" type="video/mp4">
        </video> --}}

        <form action="{{route('destroy',['file' => $user->id])}}" method="POST">
            @method('DELETE')
            @csrf
            <button type="submit">Delete</button>               
          </form>
    </div>
@endsection
